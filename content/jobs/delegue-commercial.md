---
title: Délégué(e) Commercial(e)
subtitle: Alger
date: 2019-03-27 16:19:50 +0000
---
Linkom recrute un(e) délégué(e) commercial(e)

**Mission**

* Prospecter et présenter les produits de l’agence notamment la création de sites web pour les professionnels et les autres produits de l’agence.
* Communiquer quotidiennement avec la direction
* Envoyer les rapports d’activités et concurrentiels à la direction
* Participer au développent de la charte et la stratégie commerciale de l’agence en se basant sur le feedback retourné

**Profil recherché**

* Une expérience d’au moins de 5 ans dans le commercial
* Diplômé en marketing ou en commerce.
* Etre alaises avec les nouvelles technologies.
* Maîtrise parfaite de la langue française.
* Lieu de travail : Alger

**Détails**

* Horaire de travail : 40 heures par semaine Libre
* Véhiculé
* Salaire minimum net : 65.000 DZD
* Prime : Jusqu’à plus 100.000 DZD selon objectif (tableau des objectifs déclaré préalablement)
* Type de contrat : CDI avec une période d’essai de 6 mois.
* Mode de travail très flexible avec des meeting hebdomadaire.
* Rapport d’activité obligatoire (CRM).

Ecrivez-nous directement sur jobs@linkomweb.com

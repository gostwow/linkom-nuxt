---
title: Assistante de direction
subtitle: Oran
date: 2019-03-27 16:42:38 +0100
---
Linkom recrute une assistante de direction.

**Mission**

* Gérer par téléphone et emails les clients de l’agence
* Gérer les projets avec l’équipe technique
* Participer dans le développement de la stratégie et l’image commerciale de l’agence

**Profil recherché**

* Une expérience d’au moins de deux ans dans un centre d’appel est bien souhaitée
* Maîtrise parfaite des langues français et anglais
* Habitant dans les environs de Bel-Air, Oran

**Détails**

* Horaire de travail : 8 :30 à 16h30
* Salaire : 40.000 DZD Net
* Type de contrat : CDI avec une période d’essai de 6 mois.
* Cadre de travail très calme.

Ecrivez-nous directement sur jobs@linkomweb.com

---
title: Recrutement
heading: On est toujours à la recherche de gens talentueux et authentiques
jobmail : jobs@linkomweb.com
aboutUs:
  heading: À propos de nous
  text: |
    On est une petite équipe distribuée ce qui nous permet de travailler rapidement et dynamiquement. On aime attaquer des problèmes intéressants et trouver des solutions pratiques. On investit beaucoup dans des processus nous permettant de travailler plus rapidement, plus intelligemment et plus facilement sans pour autant sacrifier la qualité.

    On finit jamais d'apprendre; nous lisons, apprenons et partageons toujours nos découvertes. On aime évoluer en tant qu'individus et en tant qu'équipe.

    Nous avons un petit bureau à Oran près de Bel Air, mais nous travaillons principalement à distance. On reste synchronisé grâce à Slack et Gitlab mais on a aussi tendance à se voir une fois autour d'un café ou un bon repas pour mettre le point sur les choses et planifier les prochaines étapes.
lookingFor:
  heading: Ce qu'on recherche
  text: |-
    On a besoin de gens passionnés avec un esprit d'équipe, qui cherchent toujours à perfectionner leurs compétences, qui comprennent leurs rôles et l'impacte de leur travail dans l'équipe.

    On apprécie la transparence et l'honnêteté, les gens qui disent ce qu'ils pensent même si n'est pas l'opinion populaire. Le client est rois mais pas toujours correcte, pour cela nous devons leur apprendre avec commentaires et propositions constructives.
openPositions:
  heading: Postes disponibles
  text: Ci-dessus sont les postes qu'on recherche activement. Si vous souhaitez nous
    rejoindre mais vous ne voyez pas votre rôle!
---

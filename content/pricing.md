---
title: Nos prix
heading: Nous créons des sites web élégants, robustes et évolutifs, hébergés dans
  des environnements dédiés et sécurisés.
packs:
- title: Essentiel
  subtitle: Une simple et agréable présentation de votre activité tout en respectant
    votre charte graphique.
  bgColor: warning
  price: 230.000 DZD
  sale_price: 210.000 DZD
  features:
  - Hébergement CDN
  - 7 comptes e-mails
  - Pages et articles illimités
  - Administration complète de votre site
- title: Boutique
  subtitle: Vendez partout avec votre boutique en ligne faites aux standards du web
    actuel
  bgColor: primary
  price: 690.000 DZD
  sale_price: 
  features:
  - Hébergement WordPress
  - 10 comptes e-mails
  - Produits et Catégories illimités
  - Panier des commandes
  - Gestion des commandes
  - Comptes Clients
- title: Sur-mesure
  subtitle: Utilisez toute la force du web en adaptant des solutions technologiques
    afin de booster votre business
  bgColor: info
  price: Contactez-nous
  sale_price: 
  features:
  - Support et convention
  - Hébergement adapté
  - Application mobile
  - 'Plateforme collaboratif '
  - Stratégie digitale
  - Réseaux sociaux
faq:
  heading: Questions fréquemment posées
  items:
  - question: Que propose Linkom ?
    answer: 'Nous sommes spécialisés dans la conception, la réalisation et la maintenance
      de sites et des applications web sur mesure. Quelque soit votre projet, un nouveau
      site ou un site existant dont les performances sont insuffisantes et qui nécessite
      une mise à jour, Linkom Web est là pour vous aider a comprendre votre cible
      et concevoir le support web qui vous convient.

'
  - question: 'Depuis quand Linkom existe ? '
    answer: |-
      On fait des sites web depuis les débuts des années deux mille, ce qu’il fait que nous avons acquis une solide réputation et une grande expérience dans le domaine, nous avons vécus le développement du web et on a toujours su s’adapter à la demande locale.
      Aujourd’hui l’agence est encore plus performante que jamais et à la hauteur de tous les défis quelques soit leurs natures et leurs tailles grâce a une direction technique jeune, très motivée et perfectionnistes.
      Nous travaillons dans l’excellence.
  - question: Hébergement
    answer: "Nous choisissons l’hébergement de votre site en fonction de sa conception
      et son besoin, pour cela nous proposons principalement trois packs\n\n1. Hébergement
      CDN (content delivery network) statistique pour les sites de type Présentation
      d’entreprise comprenant : Nom de domaine / Free SSL / 7 boites mail (2Go/compte)
      / bande passante illimités / stockage de 5 Go / Accès CMS, Webmail, imap et
      smtp\n\n2. Hébergement WordPress pour les sites développés avec WordPress ou
      les boutiques en ligne WooCommerce : Nom de domaine / Free SSL / 10 boites mail
      / Bande passante illimitée / Stockage global de 100Go pour le site et toutes
      les boites mail / Accès CMS, Webmail, imap, smtp et cPanel\n\n3. Hébergement
      Vps sur mesure adapté, on utilise généralement ce genre d’hébergement pour les
      plateformes.\n\nN.B. : La première année d'hébergement est gratuite dans les
      deux pack CDN et WordPress.\n\nNous hébergeons que les sites que nous concevons.
      \n"
  - question: Sécurité
    answer: Nous prenons au sérieux la sécurité de nos sites web, pour cela nous respectons
      tous les mesures de sécurité et les standards du développement en travaillant
      avec des outils reconnus et des hébergements sécurisés.
  - question: Référencement naturel de qualité SEO
    answer: |-
      Mettez votre site sur la première ligne.
      Le référencement naturel connu aussi sous le terme technique de SEO (acronyme de l'anglais Search Engine Optimization), consiste à améliorer la visibilité d'un site Internet dans les moteurs de recherche, et permet de générer du trafic ciblé, en combinant des actions techniques (HTML) et rédactionnelles.
      Tentez l'expérience, les sites web réalisés par Linkom sont déjà inscrits sur les moteurs de recherches et respectent parfaitement les standards actuels du refinancement naturel.
  - question: Rapports des visites avec Google Analytics.
    answer: Comme chaque outil de marqueteur moderne, le plus important dans l’opération
      est bien le retour d’information pour mieux s’adapté à la demande et préparer
      les étapes prochaines. Pour cela nous utilisons un outil très puissant et fiable
      conçu par Google. Nous intégrant Google Analytics dans nos sites afin que vous
      puissiez avoir un retour détaillé sur le comportement de vos internautes, donc
      rapport des visites, visiteurs en temps réel, langues, régions, systèmes, les
      pages les plus visitées, le temps moyens des visites, et pleins d’autres vues
      …
  - question: Responsive Web Design
    answer: |-
      Indispensable, le Responsive Web design est une approche de conception de site web qui vise à l'élaboration d’un site offrant une expérience de navigation optimale pour l'utilisateur quelle que soit son appareil (Smartphone, tablettes ou écran de bureau).
      Depuis quelques temps le responsive design est sérieusement pris en considération dans le référencement sur Google.
  - question: Le Pack Essentiel
    answer: Comme indique son nom, le pack essentiel est un standard qui s’adapte
      de façon générale aux besoins de tous les types d’entreprises ou d’activités
      locales. Un site qui englobe la présentation de l’entreprise, les produits et
      les services, les promotions et les moyens de contacts. Le site vous offre une
      interface de gestion de contenu (CMS – Content Manager System) facile à manipuler
      et semblable a Word donnant une autonomie sur le contenu de votre site, vous
      pouvez donc ajouter supprimer ou modifier vos photos et textes sur votre site
      autant de fois que vous voulez sans avoir besoin d’un informaticien et sans
      frais supplémentaires.
  - question: Le Pack Boutique en linge
    answer: "Nous développons des sites web boutiques qui respectent les plus pointues
      des exigences des standards du web moderne avec les outils de gestion de client,
      gestion des produits et des commandes et beaucoup d’options et de fonctionnalités
      supplémentaires.\nOn accompagne nos clients pour qu’ils se lancent dans leur
      aventure web. "
  - question: Plateforme collaboratif
    answer: |
      La plateforme web est l’expérience web la plus extraordinaire, exprimez vos besoins de métier et nous cherchons et concevons des systèmes collaboratifs axé sur le partage efficace de l’information. Messagerie instantané interne, CRM, portail de fidélisation de client, donnez à vos clients une interface dynamique sur vos produits et services.
      Nous pouvons aussi concevoir et accompagner les startups à réaliser leurs sites et applications mobile de tout genre.
  - question: La rédaction et les photos
    answer: |-
      Nos Packs n’incluent pas la rédaction ou la photo, nous pensons que le texte et la photo sont la matière première pour la conception de tout type de support de communication et que personne ne peut décrire votre activité mieux que vous, pour cela nous attendons de nos clients qu’ils nous fournissent ce qu’ils ont afin que leurs sites soient homogènes avec leurs communications.

      Dans le cas où le client n’a pas de photos ou de textes, nous pouvons lui écrire des textes bien pensés, des slogans et lui réaliser de belles photos facturés séparément du site web. Le client est le seul responsable du contenu éditorial et des images personnelles publiées sur son site.
  - question: Les réseaux sociaux
    answer: |-
      Souvent les établissements locaux se contentent de profils ou pages dans les réseaux sociaux les plus utilisés, ce n’est pas suffisant, il faut toujours voir le site comme votre bureau ou lieu de travail et les réseaux sociaux comme votre communication pour capter du trafic vers vous ! Chez Linkom on commence par le conseil qui peut varier depuis l’amélioration de votre présence web, comment travailler avec les réseaux sociaux jusqu’à la conception de votre site ou application web.
      Enfin le réseau social c’est bien, efficace mais vous êtes dans la rue, pour investir il vous faut une adresse et un contrôle total.
  - question: Assistance
    answer: |-
      Nous vivons de la passion du web, ce qui nous a toujours permis de continuer dans ce domaine depuis plus d’une dizaines d’années, nous avons toujours su fidéliser nos clients et faire d’eux de grandes références. Pour cette raison nous s’engageons et se forçons à être présent à coté de nos clients dans toutes leurs étapes vers le web, nous répondons à toutes vos questions et nous vous conseillons pour une meilleurs utilisation du web.

      Nous somme disponible 24h/7j par email support@linkomweb.com et 8h/5j sur le 0555269726.
  - question: TVA
    answer: TVA = 0% Comme c’est mentionné dans la loi de finances complémentaire
      (LFC) 2010 (Publication du 29 août 2010, Article 32) qui instaure une exonération
      de TVA sur les frais d'accès à Internet via le téléphone fixe. Les frais liés
      à l'hébergement de serveurs web au niveau des centres de données (Data centre)
      implantés en Algérie et en .DZ (point dz). Les frais liés à la conception et
      au développement de sites web.
  - question: Quoi d’autres ?
    answer: On travaille qu’avec des clients qui respectent le travail des autres,
      si vous pensez que votre petit fils peut faire notre travail parce que vous
      êtes loin des technologies, vous êtes à la mauvaise adresse.
---

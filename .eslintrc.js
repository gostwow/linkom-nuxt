module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'standard',
    'plugin:vue/recommended'
  ],
  // add your custom rules here
  rules: {
    'vue/component-name-in-template-casing': [
      'error',
      'PascalCase' | 'kebab-case',
      {
        registeredComponentsOnly: false,
        ignores: []
      }
    ]
  }
}
import Vue from 'vue'

Vue.mixin({
  methods: {
    $cloudinary (img) {
      return `https://res.cloudinary.com/linkom/image/upload${img}`
    }
  }
})
